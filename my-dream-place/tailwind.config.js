/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors:{
        "dark-gray":"#181818",
        "light-gray":"#F2F2F2",
        "medium-gray":"#E0E0E0",
        "footer-gray":"#4F4F4F",
        "covid-warning": "#FCEFCA",
        "booking-background":"#FFF",
        "booking-primary": "#2F80ED",
      },
      fontFamily:{
        Primary: ["SF-Pro-Display, sans-serif"],
        WorkSans: ["Work Sans, sans-serif"]
      },
      container:{
        padding: "6rem",
        maxWidth: "1440px",
      },
      screens:{
        sm:"640px",
        md:"768px",
        lg:"1440px"
      },
    },
  },
  plugins: [],
}

